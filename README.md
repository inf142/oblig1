## Dear group leader, we made two vesions of this project and hope you will enjoy both!  
![hey](two-ls-make-aw-jahmilli.gif)
  

 
![yo](mittUIB.gif)
## Installations
for installing dependencies run:  
pip install -r requirements.txt


## CLI implementation
We have made two versions of CLI implementations, one where the server gives out roles randomly and one where the client can choose what role they want. Common requirements between the two implementations is that there must be an existing advisee that is free to be connected to, as the connections between clients (advisees and advisor) is one to one. This means that one advisee can only be connected to one advisor.
Right now, we've find no good way to switch between the way roles are given out other than using commenting to switch in the code both in client.py (line 39/42) and server.py (line 57/60).

### Roles given randomly:
Here, when a client connects to the server, the server checks if there are any advisees connected, and if any advisees are free to be connected to an advisor. If there are no advisees connected to the server or any free advisees, the server will give the role advisee to the client connecting to the server. Else, it uses the random.choice() function to randomly picks role and sends it to the client as a message. The client then receives the role client-side, and can then start writing. Remember to comment in and out the respective lines of code in both client.py and server.py!

### Roles chosen by client:
In this implementation, the server waits to receive a message from client that says what role the client wants. On client-side, it starts by prompting the client to choose between advisee or advisor. The client chooses and the picked role gets sent to the server as a message, where the server receives the message. If the client picks a role that doesn't exist (for example "professor"), it prints out an error on client-side saying that the role doesn't exist and to try again, where the client can choose again. Continuing on to the server, the server receives the role as a message, then goes through some checks. If all the checks pass, the connection is made. Remember to comment in and out the respective lines of code in both client.py and server.py!

### How to run
(NB! Clients choosing role are on by default!)
- Have two or more terminals open based on how many clients is wanted.
- Start server first by running 'python server.py' while in the server folder.
- On the other terminals, start clients by running 'python client.py' while in the client folder.  
- for further guidance and a demonstration click [here](https://www.youtube.com/watch?v=uWox8KZEDVI)
  
## Web version.  
In this implementation the server and the clients are ran in a coroutine.  
The server is ran through a websockets module in python, this is because the web dom uses http for handshake  
and the websocket module have support for this while still being able to use tcp in the transport layer.  
first thing that happens when the server is started is that it waits for a connection, If it gets a connection it registers  
that connection and cheks if it can connect it to another suiting user, if so a connection is made and they can now  
write messages to eachother.  
In the front end part we use a json object to update the state of the dom and to redraw the canvas when a message is recieved.  
  
### How to run
- Open a terminal and cd into the server folder. 
- Run server_confession_room.py, it should now say "Server started".  
- Now cd into the front end folder and run: flask run  
- This should now create a link you can click.  
- From there open atleast two tabs of confession room and select a role.  
- If no role is selected you can still talk with yourself :)  
- for further guidance and a demonstration click [here](https://youtu.be/hjg3cxx8Z5w)  
  
### Known bugs:  
- In the web version the text can be bigger then the text box.  
- In the web version you might have some trouble with the websockets module (it have worked on 3/4 computers we have tried it on)
  

### Link to git repo:  
https://git.app.uib.no/inf142/oblig1
