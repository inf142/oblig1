// Create a new WebSocket connection to the server
const socket = new WebSocket('ws://localhost:40');

// Handle connection open event
socket.addEventListener("open", function(event) {
  console.log("Connected to server");
});

/**
 * when an incoming message happens this will update the state 
 * to show what the message is.
 */
socket.addEventListener("message", (message) => { // message e et json objekt så må handle det på et vis.
  
  switch(message.data.slice(0,7)){ 
  case "Advisee":
         stateChange.adviseeTalk(message.data.substring(7));
         break;
       case "":
         stateChange.defaultState();
         break;
       default:
         stateChange.advisorTalk(message.data.substring(7));
         break;
  }
  console.log("Received message:", message);
  
});

// Handle connection close event
socket.addEventListener("close", function(event) {
  console.log("Connection closed");
});

// Handle connection error event
socket.addEventListener("error", function(event) {
  console.log("Connection error:", event);
});
