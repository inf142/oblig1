 /**
   * this function is used to send a message from the user to the server, and then update the state.
   * @param {*} message the message to be sendt to the server, which then handles to whom it should be sendt. 
   * @param {*} role  the role of the user.
   */
 async function sendMessage(message, role) {
     
  socket.send(message);
     switch (role) {
       case "Advisee":
         stateChange.adviseeTalk(message.toString());
         break;
       case "":
         stateChange.defaultState();
         break;
       default:
         stateChange.advisorTalk(message.toString());
         break;
     }
  }

/**
 * this is a json object used to change the state of what is shown in the websites ui.
 * it forexample is given adviseeTalk with some text it will redraw the canvas with a text box with the correct message showing.
 */
const stateChange = {
    adviseeTalk: (text) => {
        drawChat(70,-50,400,400,text,chatBoxLeftImg)
    },
    advisorTalk: (text) => {
        drawChat(87,-50,400,400,text,chatBoxRightImg)
    },
    defaultState: () => clearCanvas()
}

/**
 * this function is used for clearing the canvas and drawing the 'start picture' again this will set it to what should be the default state.
 */
const clearCanvas = () => {
  roomContext.clearRect(0,0,confessionRoom.width, confessionRoom.height);
  roomContext.drawImage(confessionRoomImg,0,0, confessionRoom.width, confessionRoom.height)
}

/**
 * 
 * @param {*} x  the x coordinate to draw the chatbox
 * @param {*} y  the y coordinate to draw the chatbox
 * @param {*} width the width of the chatbox
 * @param {*} height the height of the chatbox
 * @param {*} text the text inside the chatbox
 * @param {*} img the img of the chatbox
 */
const drawChat = (x,y, width, height, text,img) =>{
  clearCanvas();
  roomContext.drawImage(img, x,y, width,height)
  roomContext.font = '12px Arial';
  roomContext.textAlign = 'center';
  roomContext.fillStyle = 'black';
  roomContext.fillText(text,x+ width / 2,y + height /2)
  console.log("hello")
}

