let messageField = document.getElementById("messageField");
const role = document.getElementById("adviseeOrAdvisor");
const submitButton = document.getElementById("submitButton");
const confessionRoom = document.getElementById("confessionScreen");
const roomContext = confessionRoom.getContext('2d');

const confessionRoomImg = new Image()
confessionRoomImg.src = "../static/img/chatholicConfessionRoom.png";


const chatBoxLeftImg = new Image();
chatBoxLeftImg.src = "../static/img/chatbox_transparentleft.png";

const chatBoxRightImg = new Image(); 
chatBoxRightImg.src = "../static/img/chatbox_transparentright.png";

submitButton.addEventListener("click", (event) =>{
    event.preventDefault();
    if(role.value === "")
        alert("you need to select a role in the dropdown menue before sending a message.");
    else
        sendMessage(messageField.value, role.value);
    
    messageField.value = "";
})

role.addEventListener("change", () => {
    socket.send(role.value);
    stateChange.defaultState();
    role.disabled = true;
    });








