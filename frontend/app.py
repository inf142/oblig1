import os
from flask import Flask, render_template, request


app = Flask(__name__, template_folder='templates')
app.secret_key = os.urandom(20) 


@app.route("/")
@app.route("/about")
def about():
    return render_template("about.html")

@app.route("/confession-room", methods=["GET", "POST"])
def confession_room():
    confession = request.form.get('confessionBox')
    print(confession)
    return render_template("confession-room.html")

@app.route("/priest", methods=["GET", "POST"])
def priest():
    confession = request.form.get('confessionBox')
    print(confession)
    return render_template("priest.html")

if __name__ == "__main__":
    app.debug = True
    app.run(host = "0.0.0.0", port = 5000)