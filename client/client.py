############################
#####Backend for client#####
############################
from socket import create_connection
from threading import Thread
import color

sock = create_connection(('localhost', 40))

def read() -> None:
    '''
    Side-Effects:
    Continously reads in messages sent from the server and displays them to the client.

    Returns:
    None
    '''
    while True:
        try:
            message = sock.recv(1024).decode()

            # message == "" when the error of trying to pick advisor when no free advisees are registered occur server side. This is to prevent from it printing a lot of ""
            if message != "":
                print(f"'{message}'")
          
        except OSError:
            break
        
def main() -> None:
    '''
    Side-Effect:
    Main function that displays based on role and that contains the main message loop for clients.

    Returns:
    None
    '''
    # To switch between client choosing role and randomly assigning role, comment out and comment in as needed. (Remember to do the same in server.py)
    # Used for client selecting role itself:
    role = choose_role()

    # Used for random way of getting role:
    #role = get_random_role()

    Thread(target=read).start()

    print(color.style.GREEN + "Your role is: " + color.style.BOLD + role + color.style.RESET)
    print(color.style.YELLOW + "(q to quit whenever you want)" + color.style.RESET)

    if role == "advisee":
        print(color.style.UNDERLINE + "Wait for advisor to connect, present your situation then wait for advice:" + color.style.RESET)
    else:
        print(color.style.UNDERLINE + "Wait for situation from advisee:" + color.style.RESET)
    
    # Message sending loop
    while True:
        msg = input()
        sock.sendall(msg.encode())
        if msg.lower() == "q":
            print(color.style.RED + "Quitting..." + color.style.RESET)
            break

    # Close the connection
    sock.close() 

def get_random_role() -> str:
    '''
    Side-Effect:
    Gets a random picked role from the server.

    Returns:
    A role.
    '''
    role = sock.recv(1024)
    role = role.decode()
    return role

def choose_role() -> str:
    '''
    Side-Effects:
    Lets client choose what role it wants based on certain criteria.

    Returns:
    A role.
    '''
    role = input(color.style.CYAN + "Advisee or advisor?> " + color.style.RESET).lower()
    while True:
        if role == "advisee" or role == "advisor":
            break
        print(color.style.RED + "ERROR: Role doesn't exist, please try again." + color.style.RESET)
        role = input(color.style.CYAN + "Advisee or advisor?> " + color.style.RESET).lower()
    sock.sendall(role.encode())
    return role 

if __name__ == "__main__":
    main()
