############################
#####Backend for server#####
############################
from socket import create_server
from threading import Thread, Lock
from random import choice
import color

# Set of all existing roles
roles = ("advisee", "advisor")

# Dictionary of connected clients to the server
clients = {}

# Dictionary that maps the different clients to each other like this:  {advisee : advisor}
user_connection = {} 

lock = Lock()
sock = create_server(('localhost', 40))

def accept() -> None:
    '''
    Side-Effects:
    Main server loop, continously accepts connections and registers them.

    Returns:
    None
    '''
    print(color.style.UNDERLINE + "### SERVER RUNNING ###" + color.style.RESET)
    while True:
        conn, addr = sock.accept()
        print(color.style.LIGHT_GREEN + f"Connection accepted {addr}" + color.style.RESET)
        if register(conn):
            Thread(target=read, args=(conn, addr)).start()
        else:
            print(color.style.RED + f"Lost connection to {addr} due to an error." + color.style.RESET)
            conn.close()

def register(conn) -> bool:
    '''
    Parameters:
    - conn: The connection of a client.

    Side-Effects:
    Gets or picks a role for a client based on some criteria and sends the role given/picked to the client.
    Also adds and registers the connection to the clients-dictionary and connects it to the user_connections-dictionary.
    
    Returns:
    True - If registration is completed
    False - If some error has occured due to not upholding the given criteria
    '''
    # Checks if there are any free advisees ready to be connected to an advisor.
    free_advisees = [advisee for advisee, v in user_connection.items() if not v]

    # To switch between client choosing role and randomly assigning role, comment out and comment in as needed. (Remember to do the same in client.py)
    # For client choosing role:
    role = client_role_selector(conn, free_advisees)

    # For server randomly choosing role to client:
    #role = random_role_selector(free_advisees)

    if not role:
        return False

    with lock:
        clients[conn] = role
        add_connection(conn, role)

    conn.sendall(role.encode())
    print(color.style.GREEN + "+++ REGISTERED +++" + color.style.RESET)
    return True

def add_connection(conn, role) -> None:
    '''
    Parameters:
    - conn: The connection of a client.
    - role: The role given to the client.

    Side-Effects:
    Adds connection between clients based on role.
    (If role is advisee -> value in dictionary is None, if role is advisor -> adds advisor as value to an advisee).

    Returns:
    None
    '''
    if role == "advisee":
        user_connection.setdefault(conn, None)
    elif role == "advisor":
        for advisee, _ in user_connection.items():
            if not user_connection[advisee]:
                user_connection[advisee] = conn
                advisee.sendall((color.style.GREEN + "Advisor has been connected." + color.style.RESET).encode())
                break

def unregister(conn) -> None:
    '''
    Parameters:
    - conn: The connection from a client.

    Side-Effects:
    Unregisters the client by deleting the connection in clients-dictionary and user_connection.
    Also handles certain errors that can occurs based on who disconnects.

    Returns:
    None
    '''
    with lock:
        del clients[conn]

        try:
            # This happens when the advisee of the connection disconnects to the server.
            # The whole connection will be deleted so the advisor will lose its connection
            # Unsure on how to properly handle this yet, maybe just send a message to advisor that no advisee was found
            # so the client has to reconnect to the server. 
            # That should maintain the requirement of always having an advisee ready before assigning a new advisor.
            user_connection[conn].sendall((color.style.RED + "--- ERROR: Lost connection to advisee, please reconnect to server ---" + color.style.RESET).encode())
            del user_connection[conn]
        except AttributeError:
                pass
        except KeyError:
            for advisee, advisor in user_connection.items():
                if advisor == conn:
                    user_connection[advisee] = None
                    advisee.sendall((color.style.YELLOW + "Advisor has disconnected, please wait for a new advisor." + color.style.RESET).encode())
                    break


def read(conn, addr) -> None:
    '''
    Parameters:
    - conn: The connection of a client.
    - addr: The address of the client.

    Side-Effects:
    Continously receives and reads in messages from clients and writes to corresponding connection to client.
    If connection is lost, unregisters and closes the connection.

    Returns:
    None
    '''
    while True:
        data = conn.recv(1024)
        if data.decode() == "q":
            print(color.style.YELLOW + f"--- Lost connection to {addr} ---" + color.style.RESET)
            break
        write(conn, data)
    unregister(conn)
    conn.close()

def write(conn, data) -> None:
    '''
    Parameters:
    - conn: The connection of a client.
    - data: A string of data

    Side-Effects:
    Sends the data/message to the client that is connected to conn in user_connection-dictionary.

    Returns:
    None
    '''
    try:
        if clients[conn] == "advisee":
            user_connection[conn].sendall(data)
        else:
            for advisee, advisor in user_connection.items():
                if conn == advisor:
                    key = advisee
                    break
            key.sendall(data)
    except AttributeError:
        pass
    except OSError:
        unregister(conn)
        conn.close()

def client_role_selector(conn, free_advisees) -> str:
    '''
    Parameters:
    - conn: The connection of a client.
    - free_advisees: A list of advisees with no connection to an advisor in user_connection-dictionary.

    Side-Effects:
    Receives a role from client, in which the client desires to be and assigns that role to that client.
    Also handles potential errors based on given criteria.

    Returns:
    A role.
    '''
    role = conn.recv(1024).decode()
    if role == "advisor" and ((not "advisee" in clients.values()) or (not free_advisees)):
        errormsg = (color.style.RED + "ERROR: No free advisees yet, please quit out and reconnect at a later time." + color.style.RESET).encode()
        conn.sendall(errormsg)
        return
    return role

def random_role_selector(free_advisees) -> str:
    '''
    Parameters:
    - free_advisees: A list of advisees with no connection to an advisor in user_connection-dictionary.

    Side-Effects:
    Randomly selects a role based on given criteria.

    Returns:
    A role.
    '''
    if (not roles[0] in clients.values()) or not free_advisees:
        role = roles[0]
    else:
        role = choice(roles)
    return role

if __name__ == "__main__":
    accept()
