import os

# System call
os.system("")

# Class of different styles
class style():
    '''
    Class with different color styles and their corresponding color coding
    '''
    BLACK = '\033[30m'
    RED = '\033[31m'
    GREEN = '\033[32m'
    LIGHT_GREEN = "\033[1;32m"
    YELLOW = '\033[33m'
    BLUE = '\033[34m'
    MAGENTA = '\033[35m'
    CYAN = '\033[36m'
    WHITE = '\033[37m'
    UNDERLINE = '\033[4m'
    BOLD = "\033[1m"
    RESET = '\033[0m'

# Reference: 
# Answer by SimpleBinary here: https://stackoverflow.com/questions/287871/how-do-i-print-colored-text-to-the-terminal
