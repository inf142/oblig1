import asyncio
import websockets

user_links = {}
opposite_role = {"Advisor" : "Advisee", "Advisee" : "Advisor"}
class User:
    """
    A user stores its conn and role in field variables,
    also have access to chat logs through the getChatLog method.
    field variables:
    websocket-> the users connection to the websocket.
    role -> the users role.
    hasConversation -> True if user is in conversation with another user, false otherwise.
    messages -> all messages that have been sendt from the user and recived by the user.
    """

    def __init__(self, websocket :websockets, role :str):
        self.websocket= websocket
        self.role = role
        self.messages = []

    def getChatLog(self) -> str:
        """
        Return:
        returns all messages that have been sendt to the given user.
        """
        allMessages = ""
        for message in self.messages:
            allMessages += f"{message}\n"
        return allMessages


async def handle_connection(websocket :websockets) -> None:
    '''
    param websocket : the connection to be handled.
    param path : the path of the connection.
    Side-Effect:
    handles the connection to the server, registers the user and recives and sends messages.
    '''
    print(f"New connection from {websocket.remote_address}")
    user = None
    try:
        while True:
            message = await websocket.recv()
            print(f"Received message from {websocket.remote_address}: {message}")
            if user is None:
                user = User(websocket, message)
                user_links.update({user : None})
            if user_links[user] is None:  # er noe overflødig kode her som må gåes igjennom 
                link_user(user)
            else:
                await user_links[user].websocket.send(f"{user.role}{message}")

    except websockets.exceptions.ConnectionClosed:
        print(f"Connection from {websocket.remote_address} closed.")

def link_user(user :User) -> None:
    '''
    param user : the user to check if there is a suiting connection for.
    Side-Effect:
    checks if there is a user that can be linked with the current user and links it through the user_links dict.
    '''
    for users in user_links:
        if users.role != user.role and user_links[users] == None:
            user_links[users] = user 
            user_links[user] = users
            return

async def start_server() -> None:
    '''
    Side-Effect:
    Starts the server and makes handle_connection run in a coroutine.
    '''
    async with websockets.serve(handle_connection, "localhost", 40):
        print("Server started.")
        await asyncio.Future()


if __name__ == '__main__':
    asyncio.run(start_server())
